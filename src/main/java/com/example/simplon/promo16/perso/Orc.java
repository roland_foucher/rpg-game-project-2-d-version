package com.example.simplon.promo16.perso;

/**
 * Orc have standard attack and a lot of life
 */
public class Orc extends Perso {
    /**
     * constructor Orc
     */
    public Orc() {
        this.name = "Orc";
        this.weapon += "Gourdin";
        this.manaPower += "Morsure";
        this.health = 120;
        this.mana = 60;
        this.manaAttack = 40;
        this.manaCost = 20;
        this.weaponAttack = 60;
        this.maxHealth = this.health;
        this.maxMana = this.mana;
    }

}
